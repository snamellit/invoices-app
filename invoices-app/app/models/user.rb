class User < ActiveRecord::Base

  validates_uniqueness_of :username

  def admin?
    admin
  end

  def reader?
    reader
  end

end

