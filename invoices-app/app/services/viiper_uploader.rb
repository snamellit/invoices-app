require 'rsync'
require 'oci8'

class ViiperUploader

  IMPORT_FILE = "
BEGIN
   XXMLX_SUPPLIER_DOC_SCANS_UTIL.import_file_from_directory(:name, :id, :dir);
END;
"

  MAX_IMPORT_ID = "
SELECT MAX(ID) AS id
FROM APPS.XXMLX_APEX_IMPORT_FILES
"
  MAX_IMPORT_ID_FOR_FILE = "
SELECT MAX(ID) AS id
FROM APPS.XXMLX_APEX_IMPORT_FILES
WHERE FILE_ORIGINAL_NAME = :file_name
AND IMPORT_DIRECTORY_NAME = :dir_name
"
  PATH_FOR_DIR_NAME = "
SELECT directory_path as dir_path
FROM ALL_DIRECTORIES
WHERE directory_name = :dirname"

  ALL_DIR_NAMES = "
SELECT directory_name as dir_name
FROM ALL_DIRECTORIES"

  def initialize(args)
    user = InvoiceConfig.get('VIIPER_USERNAME')
    pass = InvoiceConfig.get('VIIPER_PASSWORD')
    url = InvoiceConfig.get('VIIPER_DB_URL')

    @apex_id = InvoiceConfig.get('VIIPER_APEX_ID')
    @viiper_dir = InvoiceConfig.get('VIIPER_UPLOAD_DIR')

    @rsync_prefix = InvoiceConfig.get('VIIPER_RSYNC_PREFIX')

    Rails.logger.info "creating oracle connection"
    @db = args[:oracle] || OCI8.new(user, pass, url)
  end

  def close()
    @db.logoff
  end

  def max_apex_import_id()
    id = -1
    @db.exec(MAX_IMPORT_ID) do |row|
      id = row[0]
    end
    return id
  end

  def all_dir_names()
    dir_names = []
    @db.exec(ALL_DIR_NAMES) do |row|
      dir_names.append(row[0])
    end
    return dir_names
  end


  def upload_invoice(invoice)
    Rails.logger.info("Viiper: uploading #{invoice}")
    Rails.logger.debug "working directory : #{Dir.pwd}"
    begin

      # check if file already uploaded
      if invoice_already_uploaded?(invoice)
        Rails.logger.info("File #{invoice.file_name} for #{invoice.organization.name} was already uploaded.")
      else
        if invoice_uploaded_ok?(invoice)
          # trigger import into Oracle
          trigger_import(invoice)
        end
      end

    rescue
      invoice.result = $!
    ensure
      invoice.save
    end
 end

  def invoice_already_uploaded?(invoice)
    if invoice_apex_id(invoice) > 0
      invoice.uploaded = true
      invoice.result = 'Invoice was already uploaded'
      true
    else
      false
    end
  end

  def invoice_uploaded_ok?(invoice)
    file = clean_filename(invoice)
    target = rsync_location(invoice)
    Rails.logger.info("Sending #{file} to #{target}")
    result = Rsync.run(file , target)
    if result.success?
      true
    else
      Rails.logger.error "rsync error : #{result.error}"
      invoice.result = "rsync error : #{result.error}"
      false
    end
  end

  def trigger_import(invoice)
    Rails.logger.info("Trigger viiper import for #{invoice}")
    file_name = apex_filename(invoice)
    dirname = invoice.organization.viiper_dir_name
    Rails.logger.debug("trigger import for #{file_name} in #{dirname}")
    @db.exec(IMPORT_FILE, file_name, @apex_id, dirname)
    invoice.uploaded = true
    invoice.result = 'Success'
  end

  def rsync_location(invoice)
    return @rsync_prefix + path_for_directory(invoice) + '/'
  end

  def clean_filename(invoice)
    # escape spaces in filenames
    return invoice.file_name.gsub(" ", "\\ ").gsub("(","\\(").gsub(")","\\)")
  end

  def apex_filename(invoice)
    return File.basename(clean_filename(invoice))
  end

  def invoice_apex_id(invoice)
    id = -1
    filename = apex_filename(invoice)
    dirname = invoice.organization.viiper_dir_name
    Rails.logger.debug("search id for #{filename} in #{dirname}")
    @db.exec(MAX_IMPORT_ID_FOR_FILE, filename, dirname) do |row|
      id = row[0] || -1
    end
    Rails.logger.debug("found #{id}")
    return id
  end

  def path_for_directory(invoice)
    path = "*NOT FOUND*"
    dir_name = invoice.organization.viiper_dir_name
    Rails.logger.debug("search dir_name for #{dir_name}")
    @db.exec(PATH_FOR_DIR_NAME, dir_name) do |row|
      path = row[0] || -1
    end
    Rails.logger.debug("found #{path}")
    return path
  end

end
